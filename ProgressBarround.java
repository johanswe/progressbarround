/*
Progress Barround - Most simple progress bar for all you terminallovers

USE
Call the printProgress method with the arguments x and y,
where x whould be how fare you've come, and y would be the end goal.
The method accepts _only_ double, i.e. ProgressBarround.printProgress(57.0,145.0)
*/


class ProgressBarround{
    //final variables from https://stackoverflow.com/questions/5762491/how-to-print-color-in-console-using-system-out-println
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_BLACK = "\u001B[30m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_PURPLE = "\u001B[35m";
    private static final String ANSI_CYAN = "\u001B[36m";
    private static final String ANSI_WHITE = "\u001B[37m";

     public static void printProgress(double state, double total)
    {
        int progress = (int)(state/total*100);
        final String colorCode = getColorCode(progress);

        System.out.print(colorCode + "\r");
        System.out.print("[" + progress + "%] [");

        for (int i = 1; i <= 100; i++)
        {
            if (i < progress){
                System.out.print("#");
            } else if (progress == 100)
            {
                System.out.println("]" + ANSI_RESET + "\nSuccessful!\n");
            } else{
                System.out.print("-");
            }


            if (i == 100){
                System.out.print("]");
            }
        }

        System.out.flush();
        
    }

    private static String getColorCode(int progress)
    {
        if (progress < 25){
            return ANSI_RED;
        } else if (progress < 50){
            return ANSI_YELLOW;
        } else if (progress < 75 && progress < 100) {
            return ANSI_CYAN;
        } else {
            return ANSI_GREEN;
        }
    }
}